# Docker

update all containers:
* docker compose pull	# grab  updated images
* docker compose up -d	# rebuild containers using updated images
* docker image prune	# remove unused images...

## Useful

* docker version
* docker ps --all
* docker container ls -a
* docker logs samba -f
* docker stop samba ; docker container rm samba

